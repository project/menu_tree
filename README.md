# Menu tree

This module replaces the standard widget for selecting *Parent link* on node
add and edit forms with a tree widget.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/menu_tree).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/menu_tree).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configuration is only possible via the modules settings file
`menu_tree.settings.yml`. Edit the file in you config/sync  directory and add
the bundles for which you want to use menu tree widget.

```yaml
bundles:
  - page
  - article
```


## Maintainers

Much of the code for the Twig extension was borrowed from [https://www.drupal.org/project/simplify_menu](Simplify Menu).

- Peter Törnstrand - [Peter Törnstrand](https://www.drupal.org/u/peter-t%C3%B6rnstrand)
