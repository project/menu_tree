/**
 * React on radio button changes and set value of actual menu parent element.
 */
const change = () => {
  const triggers = Array.from(document.querySelectorAll('input[type="radio"][name="menu_tree"]'));
  window.addEventListener('change', ev => {
    const elm = ev.target;
    if (triggers.includes(elm)) {
      const target = document.querySelector('input[name="menu[menu_parent]"]');
      target.value = elm.value;
    }
  }, false);
}

/**
 * Set default value of menu tree and expand active branches.
 */
const initalState = () => {
  const parent = document.querySelector('input[name="menu[menu_parent]"]');
  let elem = document.querySelector('input[name="menu_tree"][value="' + parent.value + '"]');
  elem.checked = true;
  while (elem) {
    if (elem.matches('li') && elem.firstElementChild.matches('.toggle')) {
      elem.firstElementChild.classList.add('show');
    }
    elem = elem.parentElement;
  }
}

/**
 * Add functionality for expanding and collapsing menu tree.
 */
const expandCollapse = () => {
  window.addEventListener('load', () => {
    for (let elm of document.querySelectorAll('span.toggle')) {
      elm.onclick = () => { elm.classList.toggle('show') };
    }
  });
}

expandCollapse();
initalState();
change();
