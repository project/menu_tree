<?php

namespace Drupal\menu_tree\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a tree form element.
 *
 * @FormElement("tree")
 */
class Tree extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    $info = [
      '#input' => FALSE,
      '#multiple' => FALSE,
      '#theme' => 'input__tree',
      '#pre_render' => [
        [$class, 'preRenderTree'],
      ],
      '#id' => 'menu-tree',
      '#menus' => [],
      '#exlude' => [],
      '#title' => $this->t('Parent link'),
      '#theme_wrappers' => ['form_element'],
    ];
    return $info;
  }

  /**
   * Prepares a tree render element.
   */
  public static function preRenderTree($element) {
    $element['#attached']['library'][] = 'menu_tree/menu-tree';
    return $element;
  }

}
