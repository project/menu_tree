<?php

namespace Drupal\menu_tree\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure mail settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_tree_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['menu_tree.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $bundleInfo = \Drupal::service('entity_type.bundle.info');
    $bundles = $bundleInfo->getBundleInfo('node');
    $options = [];
    foreach ($bundles as $key => $info) {
      $options[$key] = $info['label'];
    }
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bundles'),
      '#options' => $options,
      '#default_value' => $this->config('menu_tree.settings')->get('bundles'),
      '#description' => $this->t('Select the content types for which you want to enable the menu tree.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $bundles = array_filter($form_state->getValue('bundles'), function($value) {
      return $value;
    });
    $bundles = array_values($bundles);
    $this->configFactory()->getEditable('menu_tree.settings')
      ->set('bundles', $bundles)
      ->save();
  }

}
