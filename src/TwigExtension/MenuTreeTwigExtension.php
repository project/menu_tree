<?php

namespace Drupal\menu_tree\TwigExtension;

use Drupal\menu_tree\MenuTreeItems;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class MenuItemsTwigExtension.
 *
 * @package Drupal\menu_tree
 */
class MenuTreeTwigExtension extends AbstractExtension {

  /**
   * MenuItems definition.
   *
   * @var \Drupal\menu_tree\MenuTreeItems
   */
  protected $items;

  /**
   * MenuItemsTwigExtension constructor.
   *
   * @param \Drupal\menu_tree\MenuTreeItems $items
   *   The MenuItems service.
   */
  public function __construct(MenuTreeItems $items) {
    $this->items = $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('menu_tree',
        function ($menu_id = NULL, $exclude = NULL) {
          return $this->items->getMenuTree($menu_id, $exclude);
        },
        ['is_safe' => ['html']]
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return 'menu_tree';
  }

}
